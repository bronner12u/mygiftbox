<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 12/12/2016
 * Time: 17:06
 */

require_once 'vendor/autoload.php';

use \giftbox\controler\CatalogueControler as CatControler;
use Illuminate\Database\Capsule\Manager as DB;
use \Slim\Slim as slim;
use \giftbox\vue\VueCatalogue as VueCata;
use \giftbox\models\Prestation as Prestation;

$db = new DB();

$array = parse_ini_file('src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

$app = new Slim();

$app->get('/', function(){
    $contr = new CatControler();
    $contr->afficherHome();
})->name("Home");
//les routes de VueCatalogue

$app->get('/listePrestations', function(){
    $contr = new CatControler();
    $contr->listerPrestation();
})->name("ListePrestation");

$app->get('/prestation/:id', function ($id){
    $contr = new CatControler();
    $contr->detailPresta($id);
});
$app->get('/listeCategories',function(){
    $contr = new CatControler();
    $contr->listerCategorie();
})->name("ListeCat");

$app->get('/categorie/:id', function($id){
    $contr = new CatControler();
    $contr->prestParCat($id);
});

$app->get('/trier', function() {
    $contr = new CatControler();
    $contr->trieCroissant();
});

$app->get('/trierDec', function (){
    $contr = new CatControler();
    $contr->trieDecroissant();
});

//routes de CoffretControlleur

$app->get('/ajouterPrestation/:id',function($id){
    $coffret = new \giftbox\controler\CoffretControler();
    $coffret->ajoutPrestation($id);
})->name("ajoutPrest");

$app->get('/afficherCoffret/:id',function($id){
    $coffret = new \giftbox\controler\CoffretControler();
    $coffret->afficherCoffret($id);
})->name("coffret");

$app->post('/supprimerPrest/:id',function ($id){
    $coffret = new \giftbox\controler\CoffretControler();
    $coffret->supprimerPrest($id);
});

$app->get('/validerCoffret/:id',function($id){
    $coffret = new \giftbox\controler\CoffretControler();
    $coffret->validerCoffret($id);
})->name("valider");

$app->post('/validerCoffret/validation/:id', function ($id){
    $controlerCoffret = new \giftbox\controler\CoffretControler();
    $controlerCoffret->coffretValide($id);
})->name("validationPanier");

$app->post('/validerCoffret/validation/mdp/:id', function ($id){
    $controlerCoffret = new \giftbox\controler\CoffretControler();
    $controlerCoffret->ajouterMdp($id);
})->name("ajoutmdp");

//routes de VueURL
//$app->get('/urlCadeau/:id', function ($id){
  //  \giftbox\controler\URLControler::generationURL($id);
//});

$app->get('https://webetu.iutnc.univ-lorraine.fr/www/blum14u/mygiftbox/validerCoffret/validation/cadeau/:id',function($id){
    \giftbox\controler\URLControler::ouvertureCadeau($id);

});



$app->run();