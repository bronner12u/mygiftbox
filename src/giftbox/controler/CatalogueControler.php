<?php

/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 12/12/2016
 * Time: 17:25
 */

namespace giftbox\controler;
use giftbox\models\Categorie;
use \giftbox\models\Prestation as Prestation;
use giftbox\vue\VueCatalogue;


class CatalogueControler
{
    public function afficherHome(){
        $vue =new VueCatalogue();
        echo $vue->render(3);
    }
    public function listerPrestation(){
        $q = Prestation::all();
        $vue = new VueCatalogue($q);
        echo $vue->render(1);
    }


    public function detailPresta($id){
        $q = Prestation::where("id","=",$id)->first();
        $vue = new VueCatalogue($q);
        echo $vue->render(2);

    }
    public function listerCategorie(){
        $q = Categorie::all();
        $vue = new VueCatalogue($q);
        echo $vue->render(5);
    }

    public function prestParCat($id){
        $q = Categorie::where("id",$id)->first();
        $vue = new VueCatalogue($q);
        echo $vue->render(4);
    }

    public function trieCroissant(){
        $q = Prestation::orderBy("prix")->get();
        $vue = new VueCatalogue($q);
        echo $vue->render(6);
    }

    public function trieDecroissant(){
        $q = Prestation::orderBy("prix", "DESC")->get();
        $vue = new VueCatalogue($q);
        echo $vue->render(7);
    }
}