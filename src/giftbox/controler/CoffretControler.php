<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 15/12/2016
 * Time: 09:00
 */

namespace giftbox\controler;
use giftbox\models\Categorie;
use giftbox\models\Coffret;
use \giftbox\models\Prestation as Prestation;
use giftbox\vue\VueCatalogue;
use giftbox\vue\VueCoffret;
use Illuminate\Support\Facades\View;
use Slim\Slim;

class CoffretControler
{

	//Fonctionnalité 9
	public function ajoutPrestation($id){
		$q = Prestation::where("id","=",$id)->first();
		$vue = new VueCoffret($q);
		 echo $vue->render(1);
	}

	//Foncitonnalité 10
	public function afficherCoffret($id){
	    $q = Coffret::where("idcoffret","=",$id)->first();
	    $vue = new VueCoffret($q);
		echo $vue->render(2);
	}
	
	public function validerCoffret($id){
		$q = Coffret::where("idcoffret","=",$id)->first();
		$vue = new VueCoffret($q);
		echo $vue->render(3);
	}


    public function coffretValide($id){
        $q = Coffret::where("idcoffret","=",$id)->first();
	    $vue = new VueCoffret($q);
	    echo $vue->render(4);
    }

	
	public function ajouterMdp($id){
        $q = Coffret::where("idcoffret","=",$id)->first();
        $vue = new VueCoffret($q);
        echo $vue->render(5);
	}
	
	public function supprimerPrest($id){
		$q = Coffret::where("idcoffret","=",$id)->first();
		$vue = new VueCoffret($q);
		echo $vue->render(6);
	}

    public function ouvertureCadeau($id){
        $q = Coffret::where("urlCadeau",'=',$id)->first();
        $vue = new VueCoffret($q);
        $vue->render(7);
    }}