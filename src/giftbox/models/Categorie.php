<?php

/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 05/12/2016
 * Time: 11:47
 */

namespace giftbox\models;
use \Illuminate\Database\Eloquent\Model as Model;

class Categorie extends Model{
    protected $table = 'categorie';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function prestations(){
        return $this->hasMany('giftbox\models\Prestation','cat_id');
    }
}