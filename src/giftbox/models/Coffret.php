<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 06/01/2017
 * Time: 15:56
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model as Model;

class Coffret extends Model{
    protected $table = 'coffret';
    protected $primaryKey = 'idcoffret';
    public $timestamps = false;

    public function possede(){
        return $this->hasMany('giftbox\models\Possede', 'idCoffret');
    }
}