<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 10/01/2017
 * Time: 10:59
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model as Model;

class Possede extends Model{
    protected $table = 'possede';
    protected $primaryKey = 'idP';
    public $timestamps = false;


    public function prestations(){
        return $this->hasMany('giftbox\models\Prestation','id');
    }
}