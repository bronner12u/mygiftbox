<?php

/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 05/12/2016
 * Time: 11:41
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model as Model;

class Prestation extends Model{
    protected $table = 'prestation';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function categorie(){
        return $this->belongsTo('giftbox\models\Categorie','cat_id');
    }
}