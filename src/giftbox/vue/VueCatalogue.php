<?php

/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 15/12/2016
 * Time: 16:39
 */

namespace giftbox\vue;

use giftbox\models\Categorie;
use giftbox\models\Prestation;
use Slim\Slim;

class VueCatalogue
{

    private $array;

    public function __construct($arrayPresta=null){
        $this->array = $arrayPresta;
    }

    public function home(){
        $var = "<div class='container'><div class='explication'><h1>Comment ça marche ? </h1>";
        $var .= "<p>MyGiftBox.app est une application web qui permet d'offrir des coffrets-cadeaux construits sur
mesure. Un coffret cadeau est formé d'un ensemble de prestations : activités sportives,
activités culturelles, gastronomie, hébergement, visites, attentions particulières etc...</p>
<p>Par rapport à une box classique, MyGiftBox.app permet de composer le coffret à partir d'un
catalogue de prestations, en fonction de l'occasion, des goûts du destinataire ou des
contraintes de prix, dates etc...</p>
<p>Une fois le coffret composé et payé en ligne, il est offert sous la forme d'une url qui permet à
la personne qui le reçoit de découvrir son contenu et d'en bénéficier.</p></div>";
        $var.= "<div class='row explication1'>";
        $var .="<div class='col-md-6 col-xs-6 col-lg-6 col-sm-6'> <h1 style='text-align: center'>Notre Catalogue :</h1> <img class='displayed' style='margin-top: 3%; width: 500px; height: 300px' src='images/catalogueCadeaux.png'></div>   ";
        $var.= "<div class='col-md-6 col-xs-6 col-lg-6 col-sm-6'><h1 style='text-align: center'>Votre coffret cadeau :</h1> <img class='displayed' style='margin-top: 3%; width: 300px; height: 300px' src='images/coffreCadeau.jpg'></div> </div> </div> ";
        return $var;
    }

    private function listePresta(){
        $var = " <ul style='background-color: #337ab7; width: 100%' class='nav navbar-nav navbar-left'>";
        $var .="<li class='dropdown '>";
        $var .="<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'> <span class='caret'></span> Trier Par :</button>";
        $var .="<ul class='dropdown-menu'>";
        $var .="<li><a href='trier'>Trier par prix croissant</a></li>";
        $var .="<li><a href='trierDec'>Trier par prix décroissant</a></li>";
        $var .="</ul></li></ul>";
        $var .= "<center><ul class='detailPrest'>";
        foreach ($this->array as $Prest) {
            $cat = $Prest->categorie()->first();
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.="<a href='prestation/".$Prest->id."'><li class='thumbnail'>$Prest->nom</li></a>";
            $var.="<li>$Prest->prix</li>";
            $var.= "<li>$cat->nom </li>";
            $var.="<img class='tailleImage' class='vignette' src='images/$Prest->img' />";
            $var.="<a href='ajouterPrestation/".$Prest->id."'><li class='thumbnail'>Ajouter au coffret</li></a>";
            $var.="</div>";


        }
        $var.= "</ul></center>";
        return $var;
    }
    private function afficherDetailsPrestation(){
        $a =$this->array;
        $var ="<div class='container afficherDetailsPrestation'>";
        $var .= "<h1 style='text-decoration: underline'> Voici le détails de la préstation choisie : </h1>";
        $var .= $this->array->categorie()->first()->nom.'  '.$this->array->nom.'  '.$this->array->descr;
        $var  .='<center><span>';
        $var .="<img class='tailleImage imagePresta' src='../images/$a->img' />";
        $var.="<a href='../ajouterPrestation/".$a->id."'><li class='thumbnail'>Ajouter au coffret</li></a>";
        $var .='</span></center>';
        $var .= "</div>";

        return $var;
    }

    private function listCat(){

        $var = "<div class='container'><ul>";
        foreach ($this->array as $value) {
            $var.="<li><a href='categorie/".$value->id."'>$value->nom</a></li>";



        }
        $var.= "</ul></div>";
        return $var;
    }

    private function afficherPrestParCat(){
        $a =$this->array;

        $var= "La categorie est : " . $a->nom;
        $prest1Cate = $a->prestations()->get();
        $var.= "<ul>";
            foreach ($prest1Cate as $value) {
                $var.="<li><a href='../prestation/".$value->id."'>$value->nom</a></li>";
            }
            $var.= "</ul>";

        return $var;
    }


    private function trierCroissant(){
        $a = $this->array;
        $var = " <ul style='background-color: #337ab7;  margin-bottom: 2%;width: 100%' class='nav navbar-nav navbar-left'>";
        $var .="<li class='dropdown '>";
        $var .="<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'> <span class='caret'></span> Trier Par :</button>";
        $var .="<ul class='dropdown-menu'>";
        $var .="<li><a href='trier'>Trier par prix croissant</a></li>";
        $var .="<li><a href='trierDec'>Trier par prix décroissant</a></li>";
        $var .="</ul></li></ul>";
        $var .="<h1> Voici toutes les prestations que nous pouvons offrir, dans un ordre croissant celon leurs prix : </h1>";
        $var .= "<center><ul class='detailPrest'>";
        foreach ($this->array as $Prest) {
            $cat = $Prest->categorie()->first();
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.="<a href='prestation/".$Prest->id."'><li class='thumbnail'>$Prest->nom</li></a>";
            $var.="<li>$Prest->prix</li>";
            $var.= "<li>$cat->nom </li>";
            $var.="<img class='tailleImage' class='vignette' src='images/$Prest->img' />";
            $var.="<a href='../mygiftbox/ajouterPrestation/".$Prest->id."'><li class='thumbnail'>Ajouter au coffret</li></a>";
            $var.="</div>";


        }
        $var.= "</ul></center>";
        return $var;
    }

    private function trierDecroissant(){
        $a= $this->array;
        $var = " <ul style='background-color:#337ab7; margin-bottom: 2%; width: 100%' class='nav navbar-nav navbar-left'>";
        $var .="<li class='dropdown '>";
        $var .="<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'> <span class='caret'></span> Trier Par :</button>";
        $var .="<ul class='dropdown-menu'>";
        $var .="<li><a href='trier'>Trier par prix croissant</a></li>";
        $var .="<li><a href='trierDec'>Trier par prix décroissant</a></li>";
        $var .="</ul></li></ul>";
        $var .="<h1> Voici toutes les prestations que nous pouvons offrir, dans un ordre décroissant selon leurs prix : </h1>";
        $var .= "<center><ul class='detailPrest'>";
        foreach ($this->array as $Prest) {
            $cat = $Prest->categorie()->first();
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.="<a href='prestation/".$Prest->id."'><li class='thumbnail'>$Prest->nom</li></a>";
            $var.="<li>$Prest->prix</li>";
            $var.= "<li>$cat->nom </li>";
            $var.="<img class='tailleImage' class='vignette' src='images/$Prest->img' />";
            $var.="<a href='../mygiftbox/ajouterPrestation/".$Prest->id."'><li class='thumbnail'>Ajouter au coffret</li></a>";
            $var.="</div>";


        }
        $var.= "</ul></center>";
        return $var;
    }
    public function render($id=3)
    {
        switch ($id) {
            case 1:
                $content=$this->listePresta();
                break;
            case 2:
                $content=$this->afficherDetailsPrestation();
                break;
            case 3:
                $content=$this->home();
                break;
            case 4:
                $content=$this->afficherPrestParCat();
                break;
            case 5:
                $content=$this->listCat();
                break;
            case 6:
                $content=$this->trierCroissant();
                break;
            case 7:
                $content=$this->trierDecroissant();
                break;
            default:
                $content=$this->home();
        }
        $app= Slim::getInstance();
        $urlHome= $app->urlFor("Home");
        $urlListePresta =$app->urlFor("ListePrestation");
        $urlCategorie = $app->urlFor("ListeCat");
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="$urlHome/Web/style.css">
    <title>GiftBox</title>
</head>
            <body>
                
                <div class="page-header">
                <div style='background-color: blue; width:100%; height: 10px'></div>
                <a href="$urlHome"><img id="flecheColor" src="$urlHome/images/flecheRetour.svg"></a>
                     <h1 class="text-center" style="padding-top:1%">GiftBox</h1>
                </div>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                    <ul style='background-color: #28a4c9' class="nav nav-pills">
                         <li role="presentation" class="active"><a href="$urlHome">Home</a></li>
                         <li role="presentation"><a href="$urlListePresta">Prestations</a></li>
                         <li role="presentation"><a href="$urlCategorie">Catégories</a></li>
                     </ul>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                <div id='containerListeP' class="container">
                    $content
                 </div>
                 <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            </body>
<html>
END;
        return $html;

    }


}