<?php


namespace giftbox\vue;

use giftbox\models\Categorie;
use giftbox\models\Coffret;
use giftbox\models\Possede;
use giftbox\models\Prestation;
use Slim\Slim;

class VueCoffret
{
	private $arrayCoffret;
	private $app;
	
	public function __construct($array){
		$this->arrayCoffret = $array;
        $this->app = Slim::getInstance();
	}

    public function home(){
        $var = "<div class='container'><div class='explication'><h1>Comment ça marche ? </h1>";
        $var .= "<p>MyGiftBox.app est une application web qui permet d'offrir des coffrets-cadeaux construits sur
mesure. Un coffret cadeau est formé d'un ensemble de prestations : activités sportives,
activités culturelles, gastronomie, hébergement, visites, attentions particulières etc...</p>
<p>Par rapport à une box classique, MyGiftBox.app permet de composer le coffret à partir d'un
catalogue de prestations, en fonction de l'occasion, des goûts du destinataire ou des
contraintes de prix, dates etc...</p>
<p>Une fois le coffret composé et payé en ligne, il est offert sous la forme d'une url qui permet à
la personne qui le reçoit de découvrir son contenu et d'en bénéficier.</p></div>";
        $var.= "<div class='row explication1'>";
        $var .="<div class='col-md-6 col-xs-6 col-lg-6 col-sm-6'> <h1 style='text-align: center'>Notre Catalogue :</h1> <img class='displayed' style='margin-top: 3%; width: 500px; height: 300px' src='images/catalogueCadeaux.png'></div>   ";
        $var.= "<div class='col-md-6 col-xs-6 col-lg-6 col-sm-6'><h1 style='text-align: center'>Votre coffret cadeau :</h1> <img class='displayed' style='margin-top: 3%; width: 300px; height: 300px' src='images/coffreCadeau.jpg'></div> </div> </div> ";
        return $var;
    }

	private function ajouterPrestation(){
	    $prest = $this->arrayCoffret;
	    session_start();
	    if(!isset($_SESSION['coffret'])){
	        $_SESSION['coffret']=new Coffret();
	        $_SESSION['coffret']->etat = 'reste a payer';
	        $_SESSION['coffret']->save();
        }
        $possede = new Possede();
        $possede->idcoffret = $_SESSION['coffret']->idcoffret;
        $possede->id = $prest->id;
        $possede->save();
        if(!isset($_SESSION['montant'])){
            $_SESSION['montant'] = $prest->prix;
            $_SESSION['coffret']->prix = $_SESSION['montant'];
            $_SESSION['coffret']->save();
        }
        else{
            $_SESSION['montant'] = $_SESSION['montant']+$prest->prix;
            $_SESSION['coffret']->prix = $_SESSION['montant'];
            $_SESSION['coffret']->save();
        }
        $var= "<h1 id='prest'>Prestation n°".$prest->id." ajoutée au coffret</h1>";
        $var .= "<h2> Montant total du coffret = ".$_SESSION['montant']."€</h2>";
        $var .= "<div class='page-header'>";
        $var .= "<a href='../listePrestations'><img id='flecheColor' src='../images/flecheRetour.svg'></a></div>";
        $var .= "<button type='button'><a href='../afficherCoffret/".$_SESSION['coffret']->idcoffret."'>Afficher le coffret</a></button>";
        $var .= "<button type='button'><a href='../validerCoffret/".$_SESSION['coffret']->idcoffret."'>Valider le coffret</a></button>";

        return $var;
    }

	private function afficherCoffret(){
        $coffret = $this->arrayCoffret;
        $id = $coffret->idcoffret;
        $arrayPrest = Possede::where("idcoffret","=",$id)->get();
        $var = "<div class='page-header'>";
        $var .= "<a href='../listePrestations'><img id='flecheColor' src='../images/flecheRetour.svg'></a>";
        $var .= "<button type='button'><a href='../validerCoffret/".$id."'>Valider le coffret</a></button>";
        $var .="</div>";
        foreach ($arrayPrest as $possede){
            $Prest = Prestation::where("id","=",$possede->id)->first();
            $cat = $Prest->categorie()->first();
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.="<a href='../prestation/".$Prest->id."'><li class='thumbnail'>$Prest->nom</li></a>";
            $var.="<li>$Prest->prix</li>";
            $var.="<li>$Prest->descr</li>";
            $var.= "<li>$cat->nom </li>";
            $var.="<img class='tailleImage' class='vignette' src='../images/$Prest->img' />";
            $var .= "<form name='button' method='post' action='../supprimerPrest/".$id."'>";
            $var .= "<p><input type='submit' name = 'supprimer' value = 'supprimer'/></p>";
            $var .= "</form>";
            $var.="</div>";
        }
        return $var;
    }

    private function supprimerPrest(){
        $coffret = $this->arrayCoffret;
        $id = $coffret->idcoffret;
        $arrayPrest = Possede::where("idcoffret","=",$id)->get();
        $var = "<div class='page-header'>";
        $var .= "<a href='../listePrestations'><img id='flecheColor' src='../images/flecheRetour.svg'></a>";
        $var .= "<button type='button'><a href='../validerCoffret/".$id."'>Valider le coffret</a></button>";
        $var .="</div>";
        $post = $this->app->request->post('button');
        foreach ($arrayPrest as $possede){
            if(isset($post)){
                $possede->delete();
            }
            $Prest = Prestation::where("id","=",$possede->id)->first();
            $cat = $Prest->categorie()->first();
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.="<a href='../prestation/".$Prest->id."'><li class='thumbnail'>$Prest->nom</li></a>";
            $var.="<li>$Prest->prix</li>";
            $var.="<li>$Prest->descr</li>";
            $var.= "<li>$cat->nom </li>";
            $var.="<img class='tailleImage' class='vignette' src='../images/$Prest->img' />";
        }
        return $var;
    }
	
	private function validerCoffret(){
		$coffret = $this->arrayCoffret;
		$id = $coffret->idcoffret;
		$arrayPrest = Possede::where("idcoffret","=",$id)->get();
		$nbprest = count($arrayPrest);
		if($nbprest<2){
            $var= "<h1> Vous n'avez pas assez de prestations pour valider votre coffret </h1>";
        }
        else {
            $var = "<div class='page-header'>";
            $var .= "<a href='../afficherCoffret/".$id."'><img id='flecheColor' src='../images/flecheRetour.svg'></a></div>";
            $var .= "<form method='post' action='validation/".$id."'>";
            $var .= "<p>";
            $var .= "nom";
            $var .= "<input type='text' name='nom' required/>";
            $var .= "prenom";
            $var .= "<input type='text' name='prenom' required/>";
            $var .= "mail";
            $var .= "<input type='text' name='mail' required/>";
            $var .= "message";
            $var .= "<textarea name='message' rows='4' cols='30' required></textarea>";
            $var .= "<select name='choix'>";
            $var .= "<option value='classique'> Classique </option>";
            $var .= "<option value='cagnotte'> Cagnotte </option>";
            $var .= "</select>";
            $var .= "<input type='submit' value='valider'/>";
            $var .= "</p>";
            $var .= "</form>";
        }
        return $var;
	}

	public function coffretValide(){
        $postNom = $this->app->request->post('nom');
        $postPrenom = $this->app->request->post('prenom');
        $postMail = $this->app->request->post('mail');
        $postMessage = $this->app->request->post('message');
        $postPaiement = $this->app->request->post('choix');

        $coffret = $this->arrayCoffret;
        $id = $coffret->idcoffret;
        $coffret->nomint = $postNom;
        $coffret->prenom = $postPrenom;
        $coffret->mail = $postMail;
        $coffret->message = $postMessage;
        $coffret->paiement = $postPaiement;
        $coffret->save();

        if($coffret->urlGestion == NULL){
            $coffret->urlGestion = bin2hex(openssl_random_pseudo_bytes(16));
            $coffret->save();
        }

        if($coffret->paiement == 'classique'){
            $coffret->etat = 'payé';
            $coffret->save();
        }

        $var = "<div class='page-header'>";
        $var .= "<a href='../../afficherCoffret/".$id."'><img id='flecheColor' src='../images/flecheRetour.svg'></a></div>";
        $var .= "votre coffret a bien été validé";
        $var .= "<form method='post' action='mdp/".$id."'>";
        $var .= "<p>";
        $var .= "Ajouter un mot de passe";
        $var .= "<input type='text' name='mdp' required/>";
        $var .= "<input type='submit' value='valider'/>";

        $URL = $coffret->urlCadeau;
        $route = "../urlCadeau/" . $URL;
        if($coffret->urlCadeau == NULL) {
            if ($coffret->etat == "reste a payer") {
                $var .= "<p> Vous n'avez pas validé votre coffret </p>";
            } else {
                $URL = bin2hex(openssl_random_pseudo_bytes(16));
                $coffret->urlCadeau = $URL;
                $coffret->save();
                $var .= "<a href='..cadeau/" . $URL . "'>$route</a>";
            }
        } else{
            $var .= "<a href='cadeau/" . $URL . "'>$route</a>";
        }

        return $var;
    }

    public function ajouterMdp(){
        $postmdp = $this->app->request->post('mdp');
        $coffret = $this->arrayCoffret;
        $coffret->mdp = $postmdp;
        $coffret->save();

        $var = "<h1> Le mot de passe a bien été ajouté </h1>";
        return $var;
    }

    public function ouvrirCadeau(){
        $coffret = $this->array;
        $id = $coffret->idcoffret;
        $arrayPrest = Possede::where("idcoffret","=",$id)->get();
        foreach ($arrayPrest as $possede){
            $Prest = Prestation::where("id","=",$possede->id)->first();
            $cat = $Prest->categorie()->first();
            $var = "<ul>";
            $var .= "<li>$Prest->nom</li>"."<li>$cat->nom </li>"."<img class='vignette' src='$Prest->img' />";
            $var .= "</ul>";
        }

        return $var;
    }


    public function render($id)
    {
        switch ($id) {
            case 1:
                $content = $this->ajouterPrestation();
                break;
            case 2:
                $content = $this->afficherCoffret();
				break;
            case 3:
                $content = $this->validerCoffret();
                break;
            case 4:
                $content = $this->coffretValide();
                break;
            case 5:
                $content = $this->ajouterMdp();
                break;
            case 6:
                $content = $this->supprimerPrest();
                $content = $this->afficherCoffret();
                break;
            case 7:
                $content = $this->ouvrirCadeau();
                break;
            default:
                $content=$this->home();
        }
        $app= Slim::getInstance();
        $urlHome= $app->urlFor("Home");
        $urlListePresta =$app->urlFor("ListePrestation");
        $urlCategorie = $app->urlFor("ListeCat");
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="$urlHome/Web/style.css">
    <title>GiftBox</title>
</head>
            <body>
                
                <div class="page-header">
                <div style='background-color: blue; width:100%; height: 10px'></div>
                
                     <h1 class="text-center" style="padding-top:1%">GiftBox</h1>
                </div>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                    <ul style='background-color: #28a4c9' class="nav nav-pills">
                         <li role="presentation" class="active"><a href="$urlHome">Home</a></li>
                         <li role="presentation"><a href="$urlListePresta">Prestations</a></li>
                         <li role="presentation"><a href="$urlCategorie">Catégories</a></li>
                     </ul>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                <div id='containerListeP' class="container">
                    $content
                 </div>
                 <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            </body>
<html>
END;
        return $html;

    }

}