<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 05/12/2016
 * Time: 11:48
 */
require_once 'vendor/autoload.php';

use \giftbox\models\Prestation as Prestation;
use \giftbox\models\Categorie as Categorie;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();

$array = parse_ini_file('src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

$q1 = Prestation::first();
echo $q1."<br>";
echo "nom de la categorie de la perstation: ".$q1->categorie()->first()->nom."<br>";

$q2 = Categorie::get();
echo "<br>"."c'est la liste des categories"."<br>".$q2."<br>"."<br>";

$idURL = $_GET['id'];
$q3 = Prestation::where('id','=',$idURL)->first();
echo "Prestation dont l'id est égal à ".$idURL." : ".$q3."<br> ";

$id2URL = $_GET['cat_id'];
$q4 = Categorie::where('id','=',$id2URL)->first();
$arrayPresta = $q4->prestations()->get();
echo "<br>"."La categorie '".$q4->nom."' a pour prestations : <br>".$arrayPresta;


//$presta = new Prestation();
//$presta->nom='Jeux de societe';
//$presta->descr='jeux pour tous les ages';
//$presta->cat_id=2;
//$presta->img='jeuxsociete.jpg';
//$presta->prix='25.00';
//$presta->save();

//echo "<br>"."La nouvelle prestation '".$presta->nom."' a été ajoutée à la table";